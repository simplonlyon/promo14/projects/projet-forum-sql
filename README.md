# Projet Forum SQL

L'objectif de ce projet va être de concevoir et mettre en place la base de données pour une application de forum.

## Réalisations attendues
Il va falloir faire la conception de l'application forum, créer un script de mise en place de la base de données, créer un script de mise en place de données de tests et créer un repository par entité

## Organisation
Par groupe de 3 ou 4 :
1. Choisir une thématique pour votre forum, ou rester sur un forum "générique"
2. Lister toutes les fonctionnalités qui seront possibles sur le forum, éventuellement avec un diagramme de Use Case
3. Créer 3 ou 4 user stories pour certaines fonctionnalités
4. Créer 3 ou 4 maquettes fonctionnelles de l'application
5. Identifier les "entités" de votre forum (ce qui persistera en base de données, les tables), pourquoi pas sous forme d'un diagramme de classes
6. Créer un/des script(s) SQL pour créer les différentes tables et un jeu de données pour votre application
7. Créer un projet node.js avec mysql2 puis coder un repository/DAO pour chacune des tables/entités

## A Faire en plus
Créer une application express et quelques routes pour vos repository. (Pas trop s'occuper de la partie authentification)
En gros, on peut se dire que chaque personne du groupe fait un router lié à un des repository.
